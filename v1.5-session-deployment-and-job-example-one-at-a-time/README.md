# Submit session + job to Flink in two different steps

Logs of pods were dumped to `logs-*txt`.


To deploy locally:

    $ wget https://raw.githubusercontent.com/apache/flink-kubernetes-operator/release-1.5/examples/basic-session-deployment-and-job.yaml
    $ cp basic-session-deployment-and-job.yaml basic-session-only.yaml
    $ cp basic-session-deployment-and-job.yaml basic-jobs-only.yaml
    $ rm basic-session-deployment-and-job.yaml 
    $ vim basic-session-only.yaml
    $ vim basic-jobs-only.yaml
    $ head -n 999 basic-session-only.yaml basic-jobs-only.yaml
    ==> basic-session-only.yaml <==
    apiVersion: flink.apache.org/v1beta1
    kind: FlinkDeployment
    metadata:
      name: basic-session-deployment-example
    spec:
      image: flink:1.16
      flinkVersion: v1_16
      jobManager:
        resource:
          memory: "2048m"
          cpu: 1
      taskManager:
        resource:
          memory: "2048m"
          cpu: 1
      serviceAccount: flink
    
    ==> basic-jobs-only.yaml <==
    apiVersion: flink.apache.org/v1beta1
    kind: FlinkSessionJob
    metadata:
      name: basic-session-job-example
    spec:
      deploymentName: basic-session-deployment-example
      job:
        jarURI: https://repo1.maven.org/maven2/org/apache/flink/flink-examples-streaming_2.12/1.16.1/flink-examples-streaming_2.12-1.16.1-TopSpeedWindowing.jar
        parallelism: 4
        upgradeMode: stateless
    
    ---
    apiVersion: flink.apache.org/v1beta1
    kind: FlinkSessionJob
    metadata:
      name: basic-session-job-example2
    spec:
      deploymentName: basic-session-deployment-example
      job:
        jarURI: https://repo1.maven.org/maven2/org/apache/flink/flink-examples-streaming_2.12/1.16.1/flink-examples-streaming_2.12-1.16.1.jar
        parallelism: 2
        upgradeMode: stateless
        entryClass: org.apache.flink.streaming.examples.statemachine.StateMachineExample


# Deploy the empty Flink session

    $ kubectl create -f basic-session-only.yaml 
    flinkdeployment.flink.apache.org/basic-session-deployment-example created

    $ kubectl get pods
    NAME                                               READY   STATUS    RESTARTS   AGE
    flink-kubernetes-operator-6b659d866-95f6p          2/2     Running   0          44m
    basic-session-deployment-example-b47fc8744-9psxd   1/1     Running   0          3m34s

    $ kubectl get events -A -w
    default        3m57s       Normal    ScalingReplicaSet                deployment/basic-session-deployment-example             Scaled up replica set basic-session-deployment-example-b47fc8744 to 1
    default        3m57s       Normal    SuccessfulCreate                 replicaset/basic-session-deployment-example-b47fc8744   Created pod: basic-session-deployment-example-b47fc8744-9psxd
    default        3m56s       Normal    Scheduled                        pod/basic-session-deployment-example-b47fc8744-9psxd    Successfully assigned default/basic-session-deployment-example-b47fc8744-9psxd to k3s-server
    default        3m56s       Normal    Pulling                          pod/basic-session-deployment-example-b47fc8744-9psxd    Pulling image "flink:1.16"
    default        116s        Normal    Pulled                           pod/basic-session-deployment-example-b47fc8744-9psxd    Successfully pulled image "flink:1.16" in 2m0.007565147s (2m0.007575763s including waiting)
    default        116s        Normal    Created                          pod/basic-session-deployment-example-b47fc8744-9psxd    Created container flink-main-container
    default        116s        Normal    Started                          pod/basic-session-deployment-example-b47fc8744-9psxd    Started container flink-main-container

The new resources created:

    $ ls -1rt tmp/*.csv | tail -n2 | xargs diff -u  | egrep '^\+' | egrep -v '^\+\+\+' | cut -c 2- | tr '\t' ,
    +-----------------------------------+---------+--------------------------------------------------+
    | resource                          | ns      | name                                             |
    +-----------------------------------+---------+--------------------------------------------------+
    | configmaps                        | default | flink-config-basic-session-deployment-example    |
    | deployments.apps                  | default | basic-session-deployment-example                 |
    | endpoints                         | default | basic-session-deployment-example                 |
    | endpoints                         | default | basic-session-deployment-example-rest            |
    | endpointslices.discovery.k8s.io   | default | basic-session-deployment-example-lhdgw           |
    | endpointslices.discovery.k8s.io   | default | basic-session-deployment-example-rest-9fr88      |
    | flinkdeployments.flink.apache.org | default | basic-session-deployment-example                 |
    | pods                              | default | basic-session-deployment-example-b47fc8744-9psxd |
    | pods.metrics.k8s.io               | default | basic-session-deployment-example-b47fc8744-9psxd |
    | replicasets.apps                  | default | basic-session-deployment-example-b47fc8744       |
    | services                          | default | basic-session-deployment-example                 |
    | services                          | default | basic-session-deployment-example-rest            |
    +-----------------------------------+---------+--------------------------------------------------+

# Deploy Flink jobs

    $ kubectl create -f basic-jobs-only.yaml
    flinksessionjob.flink.apache.org/basic-session-job-example created
    flinksessionjob.flink.apache.org/basic-session-job-example2 created

    $ kubectl get pods
    NAME                                               READY   STATUS    RESTARTS   AGE
    flink-kubernetes-operator-6b659d866-95f6p          2/2     Running   0          72m
    basic-session-deployment-example-b47fc8744-9psxd   1/1     Running   0          31m
    basic-session-deployment-example-taskmanager-1-3   1/1     Running   0          18s
    basic-session-deployment-example-taskmanager-1-6   1/1     Running   0          18s
    basic-session-deployment-example-taskmanager-1-4   1/1     Running   0          18s
    basic-session-deployment-example-taskmanager-1-2   1/1     Running   0          18s
    basic-session-deployment-example-taskmanager-1-1   1/1     Running   0          18s
    basic-session-deployment-example-taskmanager-1-5   1/1     Running   0          18s

    $ kubectl get events -A -w
    default     0s          Normal    Scheduled             pod/basic-session-deployment-example-taskmanager-1-3    Successfully assigned default/basic-session-deployment-example-taskmanager-1-3 to k3s-server
    default     0s          Normal    Scheduled             pod/basic-session-deployment-example-taskmanager-1-1    Successfully assigned default/basic-session-deployment-example-taskmanager-1-1 to k3s-server
    default     0s          Normal    Scheduled             pod/basic-session-deployment-example-taskmanager-1-4    Successfully assigned default/basic-session-deployment-example-taskmanager-1-4 to k3s-server
    default     0s          Normal    Scheduled             pod/basic-session-deployment-example-taskmanager-1-2    Successfully assigned default/basic-session-deployment-example-taskmanager-1-2 to k3s-server
    default     0s          Normal    Scheduled             pod/basic-session-deployment-example-taskmanager-1-5    Successfully assigned default/basic-session-deployment-example-taskmanager-1-5 to k3s-server
    default     0s          Normal    Scheduled             pod/basic-session-deployment-example-taskmanager-1-6    Successfully assigned default/basic-session-deployment-example-taskmanager-1-6 to k3s-server
    default     0s          Normal    Pulled                pod/basic-session-deployment-example-taskmanager-1-3    Container image "flink:1.16" already present on machine
    default     0s          Normal    Created               pod/basic-session-deployment-example-taskmanager-1-3    Created container flink-main-container
    default     0s          Normal    Started               pod/basic-session-deployment-example-taskmanager-1-3    Started container flink-main-container
    default     0s          Normal    Pulled                pod/basic-session-deployment-example-taskmanager-1-1    Container image "flink:1.16" already present on machine
    default     0s          Normal    Created               pod/basic-session-deployment-example-taskmanager-1-1    Created container flink-main-container
    default     0s          Normal    Started               pod/basic-session-deployment-example-taskmanager-1-1    Started container flink-main-container
    default     0s          Normal    Pulled                pod/basic-session-deployment-example-taskmanager-1-2    Container image "flink:1.16" already present on machine
    default     0s          Normal    Pulled                pod/basic-session-deployment-example-taskmanager-1-4    Container image "flink:1.16" already present on machine
    default     0s          Normal    Created               pod/basic-session-deployment-example-taskmanager-1-2    Created container flink-main-container
    default     0s          Normal    Created               pod/basic-session-deployment-example-taskmanager-1-4    Created container flink-main-container
    default     0s          Normal    Started               pod/basic-session-deployment-example-taskmanager-1-2    Started container flink-main-container
    default     0s          Normal    Started               pod/basic-session-deployment-example-taskmanager-1-4    Started container flink-main-container
    default     0s          Normal    Pulled                pod/basic-session-deployment-example-taskmanager-1-6    Container image "flink:1.16" already present on machine
    default     0s          Normal    Created               pod/basic-session-deployment-example-taskmanager-1-6    Created container flink-main-container
    default     0s          Normal    Started               pod/basic-session-deployment-example-taskmanager-1-6    Started container flink-main-container
    default     0s          Normal    Pulled                pod/basic-session-deployment-example-taskmanager-1-5    Container image "flink:1.16" already present on machine
    default     0s          Normal    Created               pod/basic-session-deployment-example-taskmanager-1-5    Created container flink-main-container
    default     0s          Normal    Started               pod/basic-session-deployment-example-taskmanager-1-5    Started container flink-main-container
    default     0s          Normal    JobStatusChanged      flinksessionjob/basic-session-job-example2              Job status changed from RECONCILING to RUNNING
    default     0s          Normal    JobStatusChanged      flinksessionjob/basic-session-job-example               Job status changed from RECONCILING to RUNNING

The new resources created:

    $ ls -1rt tmp/*.csv | tail -n2 | xargs diff -u  | egrep '^\+' | egrep -v '^\+\+\+' | cut -c 2- | tr '\t' ,
    +-----------------------------------+---------+--------------------------------------------------+
    | resource                          | ns      | name                                             |
    +-----------------------------------+---------+--------------------------------------------------+
    | flinksessionjobs.flink.apache.org | default | basic-session-job-example                        |
    | flinksessionjobs.flink.apache.org | default | basic-session-job-example2                       |
    | pods                              | default | basic-session-deployment-example-taskmanager-1-1 |
    | pods                              | default | basic-session-deployment-example-taskmanager-1-2 |
    | pods                              | default | basic-session-deployment-example-taskmanager-1-3 |
    | pods                              | default | basic-session-deployment-example-taskmanager-1-4 |
    | pods                              | default | basic-session-deployment-example-taskmanager-1-5 |
    | pods                              | default | basic-session-deployment-example-taskmanager-1-6 |
    | pods.metrics.k8s.io               | default | basic-session-deployment-example-taskmanager-1-1 |
    | pods.metrics.k8s.io               | default | basic-session-deployment-example-taskmanager-1-2 |
    | pods.metrics.k8s.io               | default | basic-session-deployment-example-taskmanager-1-3 |
    | pods.metrics.k8s.io               | default | basic-session-deployment-example-taskmanager-1-4 |
    | pods.metrics.k8s.io               | default | basic-session-deployment-example-taskmanager-1-5 |
    | pods.metrics.k8s.io               | default | basic-session-deployment-example-taskmanager-1-6 |
    +-----------------------------------+---------+--------------------------------------------------+

```mermaid
sequenceDiagram
    rect rgb(200, 200, 200)
    FlinkOperator->>Kubernetes: get_custom_resourcces()
    Note over FlinkOperator,Kubernetes: operator constantly poll for CR
    Kubernetes-->>FlinkOperator: returns: FlinkDeployment[CR]
    FlinkOperator->>FlinkDeployment[CR]: describe()
    FlinkDeployment[CR]-->>FlinkOperator: description
    FlinkOperator->>Kubernetes: create_pod(FlinkSession)
    Note over FlinkOperator,Kubernetes: creates new POD run the Flink Session
    Kubernetes->>FlinkSession[POD]: create()
    end

    rect rgb(200, 200, 200)
    FlinkOperator->>Kubernetes: get_custom_resourcces()
    Kubernetes-->>FlinkOperator: returns: FlinkSessionJob
    FlinkOperator->>FlinkSessionJob[CR]: describe()
    FlinkSessionJob[CR]-->>FlinkOperator: description

    FlinkOperator->>FlinkSession[POD]: submit_job()
    FlinkSession[POD]->>Kubernetes: create_pod(TaskManager)
    Note over FlinkSession[POD],Kubernetes: creates new POD to run TaskManager
    Kubernetes->>TaskManager: create()
    TaskManager-->>Kubernetes: return
    TaskManager->>FlinkSession[POD]: register()
    end
```