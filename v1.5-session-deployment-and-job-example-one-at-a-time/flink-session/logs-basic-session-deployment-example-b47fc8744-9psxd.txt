sed: couldn't open temporary file /opt/flink/conf/sedmBj7QD: Read-only file system
sed: couldn't open temporary file /opt/flink/conf/sedkdeMIg: Read-only file system
/docker-entrypoint.sh: line 73: /opt/flink/conf/flink-conf.yaml: Read-only file system
/docker-entrypoint.sh: line 89: /opt/flink/conf/flink-conf.yaml.tmp: Read-only file system
Starting kubernetes-session as a console application on host basic-session-deployment-example-b47fc8744-9psxd.
2023-07-28 09:19:11,754 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] - --------------------------------------------------------------------------------
2023-07-28 09:19:11,756 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] -  Preconfiguration: 
2023-07-28 09:19:11,756 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] - 


RESOURCE_PARAMS extraction logs:
jvm_params: -Xmx1530082096 -Xms1530082096 -XX:MaxMetaspaceSize=268435456
dynamic_configs: -D jobmanager.memory.off-heap.size=134217728b -D jobmanager.memory.jvm-overhead.min=214748368b -D jobmanager.memory.jvm-metaspace.size=268435456b -D jobmanager.memory.heap.size=1530082096b -D jobmanager.memory.jvm-overhead.max=214748368b
logs: WARNING: sun.reflect.Reflection.getCallerClass is not supported. This will impact performance.
INFO  [] - Loading configuration property: blob.server.port, 6124
INFO  [] - Loading configuration property: taskmanager.memory.process.size, 2048m
INFO  [] - Loading configuration property: kubernetes.jobmanager.annotations, flinkdeployment.flink.apache.org/generation:2
INFO  [] - Loading configuration property: kubernetes.internal.jobmanager.entrypoint.class, org.apache.flink.kubernetes.entrypoint.KubernetesSessionClusterEntrypoint
INFO  [] - Loading configuration property: kubernetes.jobmanager.replicas, 1
INFO  [] - Loading configuration property: jobmanager.rpc.address, basic-session-deployment-example.default
INFO  [] - Loading configuration property: web.cancel.enable, false
INFO  [] - Loading configuration property: execution.target, kubernetes-session
INFO  [] - Loading configuration property: jobmanager.memory.process.size, 2048m
INFO  [] - Loading configuration property: kubernetes.taskmanager.cpu, 1.0
INFO  [] - Loading configuration property: kubernetes.service-account, flink
INFO  [] - Loading configuration property: kubernetes.cluster-id, basic-session-deployment-example
INFO  [] - Loading configuration property: taskmanager.rpc.port, 6122
INFO  [] - Loading configuration property: internal.cluster.execution-mode, NORMAL
INFO  [] - Loading configuration property: kubernetes.container.image, flink:1.16
INFO  [] - Loading configuration property: parallelism.default, 1
INFO  [] - Loading configuration property: kubernetes.jobmanager.cpu, 1.0
INFO  [] - Loading configuration property: kubernetes.namespace, default
INFO  [] - Loading configuration property: taskmanager.numberOfTaskSlots, 1
INFO  [] - Loading configuration property: kubernetes.rest-service.exposed.type, ClusterIP
INFO  [] - Loading configuration property: kubernetes.jobmanager.owner.reference, uid:bb1bdb31-2261-45de-91fe-87281896ffb0,name:basic-session-deployment-example,controller:false,blockOwnerDeletion:true,apiVersion:flink.apache.org/v1beta1,kind:FlinkDeployment
INFO  [] - Loading configuration property: $internal.flink.version, v1_16
INFO  [] - Final Master Memory configuration:
INFO  [] -   Total Process Memory: 2.000gb (2147483648 bytes)
INFO  [] -     Total Flink Memory: 1.550gb (1664299824 bytes)
INFO  [] -       JVM Heap:         1.425gb (1530082096 bytes)
INFO  [] -       Off-heap:         128.000mb (134217728 bytes)
INFO  [] -     JVM Metaspace:      256.000mb (268435456 bytes)
INFO  [] -     JVM Overhead:       204.800mb (214748368 bytes)

2023-07-28 09:19:11,756 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] - --------------------------------------------------------------------------------
2023-07-28 09:19:11,756 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] -  Starting KubernetesSessionClusterEntrypoint (Version: 1.16.2, Scala: 2.12, Rev:fb4d4ef, Date:2023-05-17T12:01:35+02:00)
2023-07-28 09:19:11,756 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] -  OS current user: flink
2023-07-28 09:19:11,756 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] -  Current Hadoop/Kerberos user: <no hadoop dependency found>
2023-07-28 09:19:11,756 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] -  JVM: OpenJDK 64-Bit Server VM - Eclipse Adoptium - 11/11.0.20+8
2023-07-28 09:19:11,756 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] -  Arch: amd64
2023-07-28 09:19:11,757 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] -  Maximum heap size: 1411 MiBytes
2023-07-28 09:19:11,757 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] -  JAVA_HOME: /opt/java/openjdk
2023-07-28 09:19:11,757 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] -  No Hadoop Dependency available
2023-07-28 09:19:11,757 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] -  JVM Options:
2023-07-28 09:19:11,757 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] -     -Xmx1530082096
2023-07-28 09:19:11,757 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] -     -Xms1530082096
2023-07-28 09:19:11,757 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] -     -XX:MaxMetaspaceSize=268435456
2023-07-28 09:19:11,757 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] -     -Dlog.file=/opt/flink/log/flink--kubernetes-session-0-basic-session-deployment-example-b47fc8744-9psxd.log
2023-07-28 09:19:11,757 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] -     -Dlog4j.configuration=file:/opt/flink/conf/log4j-console.properties
2023-07-28 09:19:11,757 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] -     -Dlog4j.configurationFile=file:/opt/flink/conf/log4j-console.properties
2023-07-28 09:19:11,757 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] -     -Dlogback.configurationFile=file:/opt/flink/conf/logback-console.xml
2023-07-28 09:19:11,757 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] -  Program Arguments:
2023-07-28 09:19:11,758 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] -     -D
2023-07-28 09:19:11,758 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] -     jobmanager.memory.off-heap.size=134217728b
2023-07-28 09:19:11,758 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] -     -D
2023-07-28 09:19:11,758 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] -     jobmanager.memory.jvm-overhead.min=214748368b
2023-07-28 09:19:11,758 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] -     -D
2023-07-28 09:19:11,758 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] -     jobmanager.memory.jvm-metaspace.size=268435456b
2023-07-28 09:19:11,759 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] -     -D
2023-07-28 09:19:11,759 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] -     jobmanager.memory.heap.size=1530082096b
2023-07-28 09:19:11,759 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] -     -D
2023-07-28 09:19:11,759 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] -     jobmanager.memory.jvm-overhead.max=214748368b
2023-07-28 09:19:11,759 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] -  Classpath: /opt/flink/lib/flink-cep-1.16.2.jar:/opt/flink/lib/flink-connector-files-1.16.2.jar:/opt/flink/lib/flink-csv-1.16.2.jar:/opt/flink/lib/flink-json-1.16.2.jar:/opt/flink/lib/flink-scala_2.12-1.16.2.jar:/opt/flink/lib/flink-shaded-zookeeper-3.5.9.jar:/opt/flink/lib/flink-table-api-java-uber-1.16.2.jar:/opt/flink/lib/flink-table-planner-loader-1.16.2.jar:/opt/flink/lib/flink-table-runtime-1.16.2.jar:/opt/flink/lib/log4j-1.2-api-2.17.1.jar:/opt/flink/lib/log4j-api-2.17.1.jar:/opt/flink/lib/log4j-core-2.17.1.jar:/opt/flink/lib/log4j-slf4j-impl-2.17.1.jar:/opt/flink/lib/flink-dist-1.16.2.jar::::
2023-07-28 09:19:11,759 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] - --------------------------------------------------------------------------------
2023-07-28 09:19:11,760 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] - Registered UNIX signal handlers for [TERM, HUP, INT]
2023-07-28 09:19:11,767 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: blob.server.port, 6124
2023-07-28 09:19:11,768 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: taskmanager.memory.process.size, 2048m
2023-07-28 09:19:11,768 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: kubernetes.jobmanager.annotations, flinkdeployment.flink.apache.org/generation:2
2023-07-28 09:19:11,768 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: kubernetes.internal.jobmanager.entrypoint.class, org.apache.flink.kubernetes.entrypoint.KubernetesSessionClusterEntrypoint
2023-07-28 09:19:11,768 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: kubernetes.jobmanager.replicas, 1
2023-07-28 09:19:11,768 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: jobmanager.rpc.address, basic-session-deployment-example.default
2023-07-28 09:19:11,768 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: web.cancel.enable, false
2023-07-28 09:19:11,768 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: execution.target, kubernetes-session
2023-07-28 09:19:11,768 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: jobmanager.memory.process.size, 2048m
2023-07-28 09:19:11,768 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: kubernetes.taskmanager.cpu, 1.0
2023-07-28 09:19:11,768 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: kubernetes.service-account, flink
2023-07-28 09:19:11,768 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: kubernetes.cluster-id, basic-session-deployment-example
2023-07-28 09:19:11,768 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: taskmanager.rpc.port, 6122
2023-07-28 09:19:11,768 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: internal.cluster.execution-mode, NORMAL
2023-07-28 09:19:11,769 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: kubernetes.container.image, flink:1.16
2023-07-28 09:19:11,769 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: parallelism.default, 1
2023-07-28 09:19:11,769 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: kubernetes.jobmanager.cpu, 1.0
2023-07-28 09:19:11,769 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: kubernetes.namespace, default
2023-07-28 09:19:11,769 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: taskmanager.numberOfTaskSlots, 1
2023-07-28 09:19:11,769 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: kubernetes.rest-service.exposed.type, ClusterIP
2023-07-28 09:19:11,769 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: kubernetes.jobmanager.owner.reference, uid:bb1bdb31-2261-45de-91fe-87281896ffb0,name:basic-session-deployment-example,controller:false,blockOwnerDeletion:true,apiVersion:flink.apache.org/v1beta1,kind:FlinkDeployment
2023-07-28 09:19:11,769 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: $internal.flink.version, v1_16
2023-07-28 09:19:11,769 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading dynamic configuration property: jobmanager.memory.off-heap.size, 134217728b
2023-07-28 09:19:11,769 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading dynamic configuration property: jobmanager.memory.jvm-overhead.min, 214748368b
2023-07-28 09:19:11,769 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading dynamic configuration property: jobmanager.memory.jvm-metaspace.size, 268435456b
2023-07-28 09:19:11,769 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading dynamic configuration property: jobmanager.memory.heap.size, 1530082096b
2023-07-28 09:19:11,769 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading dynamic configuration property: jobmanager.memory.jvm-overhead.max, 214748368b
2023-07-28 09:19:11,901 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] - Starting KubernetesSessionClusterEntrypoint.
2023-07-28 09:19:11,964 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] - Install default filesystem.
2023-07-28 09:19:11,966 INFO  org.apache.flink.core.fs.FileSystem                          [] - Hadoop is not in the classpath/dependencies. The extended set of supported File Systems via Hadoop is not available.
2023-07-28 09:19:11,994 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] - Install security context.
2023-07-28 09:19:12,048 INFO  org.apache.flink.runtime.security.modules.HadoopModuleFactory [] - Cannot create Hadoop Security Module because Hadoop cannot be found in the Classpath.
2023-07-28 09:19:12,052 INFO  org.apache.flink.runtime.security.modules.JaasModule         [] - Jaas file will be created as /tmp/jaas-16701345099846135100.conf.
2023-07-28 09:19:12,056 INFO  org.apache.flink.runtime.security.contexts.HadoopSecurityContextFactory [] - Cannot install HadoopSecurityContext because Hadoop cannot be found in the Classpath.
2023-07-28 09:19:12,057 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] - Initializing cluster services.
2023-07-28 09:19:12,062 INFO  org.apache.flink.runtime.entrypoint.ClusterEntrypoint        [] - Using working directory: WorkingDirectory(/tmp/jm_a38c283d2918a479d582d7ad62ad2232).
2023-07-28 09:19:12,548 INFO  org.apache.flink.runtime.rpc.akka.AkkaRpcServiceUtils        [] - Trying to start actor system, external address basic-session-deployment-example.default:6123, bind address 0.0.0.0:6123.
2023-07-28 09:19:13,345 INFO  akka.event.slf4j.Slf4jLogger                                 [] - Slf4jLogger started
2023-07-28 09:19:13,367 INFO  akka.remote.RemoteActorRefProvider                           [] - Akka Cluster not in use - enabling unsafe features anyway because `akka.remote.use-unsafe-remote-features-outside-cluster` has been enabled.
2023-07-28 09:19:13,367 INFO  akka.remote.Remoting                                         [] - Starting remoting
2023-07-28 09:19:13,568 INFO  akka.remote.Remoting                                         [] - Remoting started; listening on addresses :[akka.tcp://flink@basic-session-deployment-example.default:6123]
2023-07-28 09:19:13,700 INFO  org.apache.flink.runtime.rpc.akka.AkkaRpcServiceUtils        [] - Actor system started at akka.tcp://flink@basic-session-deployment-example.default:6123
2023-07-28 09:19:13,768 INFO  org.apache.flink.configuration.Configuration                 [] - Config uses fallback configuration key 'jobmanager.rpc.address' instead of key 'rest.address'
2023-07-28 09:19:13,773 INFO  org.apache.flink.runtime.blob.BlobServer                     [] - Created BLOB server storage directory /tmp/jm_a38c283d2918a479d582d7ad62ad2232/blobStorage
2023-07-28 09:19:13,776 INFO  org.apache.flink.runtime.blob.BlobServer                     [] - Started BLOB server at 0.0.0.0:6124 - max concurrent requests: 50 - max backlog: 1000
2023-07-28 09:19:13,780 INFO  org.apache.flink.runtime.security.token.KerberosDelegationTokenManagerFactory [] - Cannot use kerberos delegation token manager because Hadoop cannot be found in the Classpath.
2023-07-28 09:19:13,786 INFO  org.apache.flink.runtime.metrics.MetricRegistryImpl          [] - No metrics reporter configured, no metrics will be exposed/reported.
2023-07-28 09:19:13,789 INFO  org.apache.flink.runtime.rpc.akka.AkkaRpcServiceUtils        [] - Trying to start actor system, external address basic-session-deployment-example.default:0, bind address 0.0.0.0:0.
2023-07-28 09:19:13,848 INFO  akka.event.slf4j.Slf4jLogger                                 [] - Slf4jLogger started
2023-07-28 09:19:13,852 INFO  akka.remote.RemoteActorRefProvider                           [] - Akka Cluster not in use - enabling unsafe features anyway because `akka.remote.use-unsafe-remote-features-outside-cluster` has been enabled.
2023-07-28 09:19:13,852 INFO  akka.remote.Remoting                                         [] - Starting remoting
2023-07-28 09:19:13,858 INFO  akka.remote.Remoting                                         [] - Remoting started; listening on addresses :[akka.tcp://flink-metrics@basic-session-deployment-example.default:41877]
2023-07-28 09:19:13,863 INFO  org.apache.flink.runtime.rpc.akka.AkkaRpcServiceUtils        [] - Actor system started at akka.tcp://flink-metrics@basic-session-deployment-example.default:41877
2023-07-28 09:19:13,872 INFO  org.apache.flink.runtime.rpc.akka.AkkaRpcService             [] - Starting RPC endpoint for org.apache.flink.runtime.metrics.dump.MetricQueryService at akka://flink-metrics/user/rpc/MetricQueryService .
2023-07-28 09:19:13,953 INFO  org.apache.flink.runtime.dispatcher.FileExecutionGraphInfoStore [] - Initializing FileExecutionGraphInfoStore: Storage directory /tmp/executionGraphStore-f426716e-f256-4c22-b189-3b6ccf122d43, expiration time 3600000, maximum cache size 52428800 bytes.
2023-07-28 09:19:13,984 INFO  org.apache.flink.configuration.Configuration                 [] - Config uses fallback configuration key 'jobmanager.rpc.address' instead of key 'rest.address'
2023-07-28 09:19:13,984 INFO  org.apache.flink.runtime.dispatcher.DispatcherRestEndpoint   [] - Upload directory /tmp/flink-web-23ba5a71-4811-4ebd-8eff-4a651e687000/flink-web-upload does not exist. 
2023-07-28 09:19:13,985 INFO  org.apache.flink.runtime.dispatcher.DispatcherRestEndpoint   [] - Created directory /tmp/flink-web-23ba5a71-4811-4ebd-8eff-4a651e687000/flink-web-upload for file uploads.
2023-07-28 09:19:13,986 INFO  org.apache.flink.runtime.dispatcher.DispatcherRestEndpoint   [] - Starting rest endpoint.
2023-07-28 09:19:14,253 INFO  org.apache.flink.runtime.webmonitor.WebMonitorUtils          [] - Determined location of main cluster component log file: /opt/flink/log/flink--kubernetes-session-0-basic-session-deployment-example-b47fc8744-9psxd.log
2023-07-28 09:19:14,253 INFO  org.apache.flink.runtime.webmonitor.WebMonitorUtils          [] - Determined location of main cluster component stdout file: /opt/flink/log/flink--kubernetes-session-0-basic-session-deployment-example-b47fc8744-9psxd.out
2023-07-28 09:19:14,448 INFO  org.apache.flink.runtime.dispatcher.DispatcherRestEndpoint   [] - Rest endpoint listening at basic-session-deployment-example.default:8081
2023-07-28 09:19:14,449 INFO  org.apache.flink.runtime.dispatcher.DispatcherRestEndpoint   [] - http://basic-session-deployment-example.default:8081 was granted leadership with leaderSessionID=00000000-0000-0000-0000-000000000000
2023-07-28 09:19:14,450 INFO  org.apache.flink.runtime.dispatcher.DispatcherRestEndpoint   [] - Web frontend listening at http://basic-session-deployment-example.default:8081.
2023-07-28 09:19:14,467 INFO  org.apache.flink.runtime.dispatcher.runner.DefaultDispatcherRunner [] - DefaultDispatcherRunner was granted leadership with leader id 00000000-0000-0000-0000-000000000000. Creating new DispatcherLeaderProcess.
2023-07-28 09:19:14,471 INFO  org.apache.flink.runtime.dispatcher.runner.SessionDispatcherLeaderProcess [] - Start SessionDispatcherLeaderProcess.
2023-07-28 09:19:14,472 INFO  org.apache.flink.runtime.resourcemanager.ResourceManagerServiceImpl [] - Starting resource manager service.
2023-07-28 09:19:14,473 INFO  org.apache.flink.runtime.resourcemanager.ResourceManagerServiceImpl [] - Resource manager service is granted leadership with session id 00000000-0000-0000-0000-000000000000.
2023-07-28 09:19:14,474 INFO  org.apache.flink.runtime.dispatcher.runner.SessionDispatcherLeaderProcess [] - Recover all persisted job graphs that are not finished, yet.
2023-07-28 09:19:14,474 INFO  org.apache.flink.runtime.dispatcher.runner.SessionDispatcherLeaderProcess [] - Successfully recovered 0 persisted job graphs.
2023-07-28 09:19:14,549 INFO  org.apache.flink.runtime.rpc.akka.AkkaRpcService             [] - Starting RPC endpoint for org.apache.flink.runtime.dispatcher.StandaloneDispatcher at akka://flink/user/rpc/dispatcher_0 .
2023-07-28 09:19:15,057 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: blob.server.port, 6124
2023-07-28 09:19:15,058 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: taskmanager.memory.process.size, 2048m
2023-07-28 09:19:15,058 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: kubernetes.jobmanager.annotations, flinkdeployment.flink.apache.org/generation:2
2023-07-28 09:19:15,058 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: kubernetes.internal.jobmanager.entrypoint.class, org.apache.flink.kubernetes.entrypoint.KubernetesSessionClusterEntrypoint
2023-07-28 09:19:15,058 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: kubernetes.jobmanager.replicas, 1
2023-07-28 09:19:15,058 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: jobmanager.rpc.address, basic-session-deployment-example.default
2023-07-28 09:19:15,058 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: web.cancel.enable, false
2023-07-28 09:19:15,058 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: execution.target, kubernetes-session
2023-07-28 09:19:15,058 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: jobmanager.memory.process.size, 2048m
2023-07-28 09:19:15,058 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: kubernetes.taskmanager.cpu, 1.0
2023-07-28 09:19:15,058 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: kubernetes.service-account, flink
2023-07-28 09:19:15,058 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: kubernetes.cluster-id, basic-session-deployment-example
2023-07-28 09:19:15,058 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: taskmanager.rpc.port, 6122
2023-07-28 09:19:15,059 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: internal.cluster.execution-mode, NORMAL
2023-07-28 09:19:15,059 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: kubernetes.container.image, flink:1.16
2023-07-28 09:19:15,059 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: parallelism.default, 1
2023-07-28 09:19:15,059 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: kubernetes.jobmanager.cpu, 1.0
2023-07-28 09:19:15,059 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: kubernetes.namespace, default
2023-07-28 09:19:15,059 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: taskmanager.numberOfTaskSlots, 1
2023-07-28 09:19:15,059 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: kubernetes.rest-service.exposed.type, ClusterIP
2023-07-28 09:19:15,059 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: kubernetes.jobmanager.owner.reference, uid:bb1bdb31-2261-45de-91fe-87281896ffb0,name:basic-session-deployment-example,controller:false,blockOwnerDeletion:true,apiVersion:flink.apache.org/v1beta1,kind:FlinkDeployment
2023-07-28 09:19:15,059 INFO  org.apache.flink.configuration.GlobalConfiguration           [] - Loading configuration property: $internal.flink.version, v1_16
2023-07-28 09:19:15,062 INFO  org.apache.flink.runtime.rpc.akka.AkkaRpcService             [] - Starting RPC endpoint for org.apache.flink.runtime.resourcemanager.active.ActiveResourceManager at akka://flink/user/rpc/resourcemanager_1 .
2023-07-28 09:19:15,068 INFO  org.apache.flink.runtime.resourcemanager.active.ActiveResourceManager [] - Starting the resource manager.
2023-07-28 09:19:16,061 INFO  org.apache.flink.kubernetes.KubernetesResourceManagerDriver  [] - Recovered 0 pods from previous attempts, current attempt id is 1.
2023-07-28 09:19:16,061 INFO  org.apache.flink.runtime.resourcemanager.active.ActiveResourceManager [] - Recovered 0 workers from previous attempt.
