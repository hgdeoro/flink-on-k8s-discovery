Name:             basic-session-deployment-example-taskmanager-1-4
Namespace:        default
Priority:         0
Service Account:  flink
Node:             k3s-server/10.10.0.133
Start Time:       Fri, 28 Jul 2023 12:48:01 +0300
Labels:           app=basic-session-deployment-example
                  component=taskmanager
                  type=flink-native-kubernetes
Annotations:      <none>
Status:           Running
IP:               10.42.0.16
IPs:
  IP:           10.42.0.16
Controlled By:  Deployment/basic-session-deployment-example
Containers:
  flink-main-container:
    Container ID:  containerd://273447d4429acd3f33f7af6335b1ff140c7a19c2bd062cddf35275299596be44
    Image:         flink:1.16
    Image ID:      docker.io/library/flink@sha256:b566594bde6c89e85aa0f545a3321ee7dc3a4b3a0f68d85be135033f79b2e1fb
    Port:          6122/TCP
    Host Port:     0/TCP
    Command:
      /docker-entrypoint.sh
    Args:
      bash
      -c
      kubernetes-taskmanager.sh -Djobmanager.memory.jvm-overhead.min='214748368b' -Dtaskmanager.resource-id='basic-session-deployment-example-taskmanager-1-4' -Djobmanager.memory.off-heap.size='134217728b' -Dweb.tmpdir='/tmp/flink-web-23ba5a71-4811-4ebd-8eff-4a651e687000' -Djobmanager.rpc.port='6123' -Djobmanager.memory.jvm-metaspace.size='268435456b' -Djobmanager.memory.heap.size='1530082096b' -Djobmanager.memory.jvm-overhead.max='214748368b' -D taskmanager.memory.network.min=166429984b -D taskmanager.cpu.cores=1.0 -D taskmanager.memory.task.off-heap.size=0b -D taskmanager.memory.jvm-metaspace.size=268435456b -D external-resources=none -D taskmanager.memory.jvm-overhead.min=214748368b -D taskmanager.memory.framework.off-heap.size=134217728b -D taskmanager.memory.network.max=166429984b -D taskmanager.memory.framework.heap.size=134217728b -D taskmanager.memory.managed.size=665719939b -D taskmanager.memory.task.heap.size=563714445b -D taskmanager.numberOfTaskSlots=1 -D taskmanager.memory.jvm-overhead.max=214748368b 
    State:          Running
      Started:      Fri, 28 Jul 2023 12:48:03 +0300
    Ready:          True
    Restart Count:  0
    Limits:
      cpu:     1
      memory:  2Gi
    Requests:
      cpu:     1
      memory:  2Gi
    Environment:
      _POD_NODE_ID:            (v1:spec.nodeName)
      FLINK_TM_JVM_MEM_OPTS:  -Xmx697932173 -Xms697932173 -XX:MaxDirectMemorySize=300647712 -XX:MaxMetaspaceSize=268435456
    Mounts:
      /opt/flink/conf from flink-config-volume (rw)
      /var/run/secrets/kubernetes.io/serviceaccount from kube-api-access-gnpmr (ro)
Conditions:
  Type              Status
  Initialized       True 
  Ready             True 
  ContainersReady   True 
  PodScheduled      True 
Volumes:
  flink-config-volume:
    Type:      ConfigMap (a volume populated by a ConfigMap)
    Name:      flink-config-basic-session-deployment-example
    Optional:  false
  kube-api-access-gnpmr:
    Type:                    Projected (a volume that contains injected data from multiple sources)
    TokenExpirationSeconds:  3607
    ConfigMapName:           kube-root-ca.crt
    ConfigMapOptional:       <nil>
    DownwardAPI:             true
QoS Class:                   Guaranteed
Node-Selectors:              <none>
Tolerations:                 node.kubernetes.io/not-ready:NoExecute op=Exists for 300s
                             node.kubernetes.io/unreachable:NoExecute op=Exists for 300s
Events:
  Type    Reason     Age    From               Message
  ----    ------     ----   ----               -------
  Normal  Scheduled  4m44s  default-scheduler  Successfully assigned default/basic-session-deployment-example-taskmanager-1-4 to k3s-server
  Normal  Pulled     4m44s  kubelet            Container image "flink:1.16" already present on machine
  Normal  Created    4m44s  kubelet            Created container flink-main-container
  Normal  Started    4m43s  kubelet            Started container flink-main-container
