Name:         basic-session-deployment-example-taskmanager-1-2
Namespace:    default
Labels:       app=basic-session-deployment-example
              component=taskmanager
              type=flink-native-kubernetes
Annotations:  <none>
API Version:  metrics.k8s.io/v1beta1
Containers:
  Name:  flink-main-container
  Usage:
    Cpu:     15716408n
    Memory:  409096Ki
Kind:        PodMetrics
Metadata:
  Creation Timestamp:  2023-07-28T09:52:46Z
Timestamp:             2023-07-28T09:52:35Z
Window:                16.693s
Events:                <none>
