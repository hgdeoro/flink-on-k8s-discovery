#!/bin/bash

set -eu

file_1="$1"
file_2="$2"

for line in $(diff -u "$file_1" "$file_2" | egrep '^\+' | egrep -v '^\+\+\+' | cut -c 2- | tr '\t' ,) ; do
  resource_type=$(echo "$line" | cut -d, -f1)
  namespace=$(echo "$line" | cut -d, -f2)
  resource_name=$(echo "$line" | cut -d, -f3)
  echo "Dumping $resource_type/$resource_name @ $namespace"
  kubectl describe -n "$namespace" "$resource_type"/"$resource_name" > tmp/describe-$(echo "$line" | tr , -).txt
done
