# Submit session + job to Flink

Logs of pods were dumped to `logs-*txt`.

To deploy locally:

    $ curl https://raw.githubusercontent.com/apache/flink-kubernetes-operator/release-1.5/examples/basic-session-deployment-and-job.yaml
    apiVersion: flink.apache.org/v1beta1
    kind: FlinkDeployment
    metadata:
      name: basic-session-deployment-example
    spec:
      image: flink:1.16
      flinkVersion: v1_16
      jobManager:
        resource:
          memory: "2048m"
          cpu: 1
      taskManager:
        resource:
          memory: "2048m"
          cpu: 1
      serviceAccount: flink
    ---
    apiVersion: flink.apache.org/v1beta1
    kind: FlinkSessionJob
    metadata:
      name: basic-session-job-example
    spec:
      deploymentName: basic-session-deployment-example
      job:
        jarURI: https://repo1.maven.org/maven2/org/apache/flink/flink-examples-streaming_2.12/1.16.1/flink-examples-streaming_2.12-1.16.1-TopSpeedWindowing.jar
        parallelism: 4
        upgradeMode: stateless
    ---
    apiVersion: flink.apache.org/v1beta1
    kind: FlinkSessionJob
    metadata:
      name: basic-session-job-example2
    spec:
      deploymentName: basic-session-deployment-example
      job:
        jarURI: https://repo1.maven.org/maven2/org/apache/flink/flink-examples-streaming_2.12/1.16.1/flink-examples-streaming_2.12-1.16.1.jar
        parallelism: 2
        upgradeMode: stateless
        entryClass: org.apache.flink.streaming.examples.statemachine.StateMachineExample

    $ kubectl create -f https://raw.githubusercontent.com/apache/flink-kubernetes-operator/release-1.5/examples/basic-session-deployment-and-job.yaml
    flinkdeployment.flink.apache.org/basic-session-deployment-example created
    flinksessionjob.flink.apache.org/basic-session-job-example created
    flinksessionjob.flink.apache.org/basic-session-job-example2 created

    $ kubectl get pods
    NAME                                               READY   STATUS    RESTARTS   AGE
    flink-kubernetes-operator-6b659d866-rpfb7          2/2     Running   0          11m
    basic-session-deployment-example-b47fc8744-zx6pf   1/1     Running   0          4m46s
    basic-session-deployment-example-taskmanager-1-4   1/1     Running   0          2m32s
    basic-session-deployment-example-taskmanager-1-3   1/1     Running   0          2m32s
    basic-session-deployment-example-taskmanager-1-1   1/1     Running   0          2m32s
    basic-session-deployment-example-taskmanager-1-2   1/1     Running   0          2m32s
    basic-session-deployment-example-taskmanager-1-6   1/1     Running   0          2m32s
    basic-session-deployment-example-taskmanager-1-5   1/1     Running   0          2m32s

    $ kubectl get events -A -w
    default        0s          Normal    ScalingReplicaSet                deployment/basic-session-deployment-example            Scaled up replica set basic-session-deployment-example-b47fc8744 to 1
    default        0s          Normal    SuccessfulCreate                 replicaset/basic-session-deployment-example-b47fc8744   Created pod: basic-session-deployment-example-b47fc8744-zx6pf
    default        0s          Normal    Scheduled                        pod/basic-session-deployment-example-b47fc8744-zx6pf    Successfully assigned default/basic-session-deployment-example-b47fc8744-zx6pf to k3s-server
    default        0s          Warning   FailedMount                      pod/basic-session-deployment-example-b47fc8744-zx6pf    MountVolume.SetUp failed for volume "flink-config-volume" : configmap "flink-config-basic-session-deployment-example" not found
    default        0s          Normal    Pulling                          pod/basic-session-deployment-example-b47fc8744-zx6pf    Pulling image "flink:1.16"
    default        0s          Normal    Pulled                           pod/basic-session-deployment-example-b47fc8744-zx6pf    Successfully pulled image "flink:1.16" in 1m55.080410211s (1m55.080431582s including waiting)
    default        0s          Normal    Created                          pod/basic-session-deployment-example-b47fc8744-zx6pf    Created container flink-main-container
    default        0s          Normal    Started                          pod/basic-session-deployment-example-b47fc8744-zx6pf    Started container flink-main-container
    default        0s          Normal    Scheduled                        pod/basic-session-deployment-example-taskmanager-1-2    Successfully assigned default/basic-session-deployment-example-taskmanager-1-2 to k3s-server
    default        0s          Normal    Scheduled                        pod/basic-session-deployment-example-taskmanager-1-4    Successfully assigned default/basic-session-deployment-example-taskmanager-1-4 to k3s-server
    default        0s          Normal    Scheduled                        pod/basic-session-deployment-example-taskmanager-1-1    Successfully assigned default/basic-session-deployment-example-taskmanager-1-1 to k3s-server
    default        0s          Normal    Scheduled                        pod/basic-session-deployment-example-taskmanager-1-3    Successfully assigned default/basic-session-deployment-example-taskmanager-1-3 to k3s-server
    default        0s          Normal    Scheduled                        pod/basic-session-deployment-example-taskmanager-1-5    Successfully assigned default/basic-session-deployment-example-taskmanager-1-5 to k3s-server
    default        0s          Normal    Scheduled                        pod/basic-session-deployment-example-taskmanager-1-6    Successfully assigned default/basic-session-deployment-example-taskmanager-1-6 to k3s-server
    default        0s          Normal    Pulled                           pod/basic-session-deployment-example-taskmanager-1-1    Container image "flink:1.16" already present on machine
    default        0s          Normal    Created                          pod/basic-session-deployment-example-taskmanager-1-1    Created container flink-main-container
    default        0s          Normal    Started                          pod/basic-session-deployment-example-taskmanager-1-1    Started container flink-main-container
    default        0s          Normal    Pulled                           pod/basic-session-deployment-example-taskmanager-1-3    Container image "flink:1.16" already present on machine
    default        0s          Normal    Created                          pod/basic-session-deployment-example-taskmanager-1-3    Created container flink-main-container
    default        0s          Normal    Started                          pod/basic-session-deployment-example-taskmanager-1-3    Started container flink-main-container
    default        0s          Normal    Pulled                           pod/basic-session-deployment-example-taskmanager-1-4    Container image "flink:1.16" already present on machine
    default        0s          Normal    Pulled                           pod/basic-session-deployment-example-taskmanager-1-2    Container image "flink:1.16" already present on machine
    default        0s          Normal    Created                          pod/basic-session-deployment-example-taskmanager-1-2    Created container flink-main-container
    default        0s          Normal    Created                          pod/basic-session-deployment-example-taskmanager-1-4    Created container flink-main-container
    default        0s          Normal    Started                          pod/basic-session-deployment-example-taskmanager-1-2    Started container flink-main-container
    default        0s          Normal    Started                          pod/basic-session-deployment-example-taskmanager-1-4    Started container flink-main-container
    default        0s          Normal    Pulled                           pod/basic-session-deployment-example-taskmanager-1-6    Container image "flink:1.16" already present on machine
    default        0s          Normal    Created                          pod/basic-session-deployment-example-taskmanager-1-6    Created container flink-main-container
    default        0s          Normal    Started                          pod/basic-session-deployment-example-taskmanager-1-6    Started container flink-main-container
    default        0s          Normal    Pulled                           pod/basic-session-deployment-example-taskmanager-1-5    Container image "flink:1.16" already present on machine
    default        0s          Normal    Created                          pod/basic-session-deployment-example-taskmanager-1-5    Created container flink-main-container
    default        0s          Normal    Started                          pod/basic-session-deployment-example-taskmanager-1-5    Started container flink-main-container
    default        0s          Normal    JobStatusChanged                 flinksessionjob/basic-session-job-example2              Job status changed from RECONCILING to RUNNING
    default        0s          Normal    JobStatusChanged                 flinksessionjob/basic-session-job-example               Job status changed from RECONCILING to RUNNING

The new resources created:

    $ ls -1rt tmp/*.csv | tail -n2 | xargs diff -u  | egrep '^\+' | egrep -v '^\+\+\+' | cut -c 2- | tr '\t' ,
    +-----------------------------------+---------+--------------------------------------------------+
    | resource                          | ns      | name                                             |
    +-----------------------------------+---------+--------------------------------------------------+
    | configmaps                        | default | flink-config-basic-session-deployment-example    |
    | deployments.apps                  | default | basic-session-deployment-example                 |
    | endpoints                         | default | basic-session-deployment-example                 |
    | endpoints                         | default | basic-session-deployment-example-rest            |
    | endpointslices.discovery.k8s.io   | default | basic-session-deployment-example-rest-v5j5k      |
    | endpointslices.discovery.k8s.io   | default | basic-session-deployment-example-tl72m           |
    | flinkdeployments.flink.apache.org | default | basic-session-deployment-example                 |
    | flinksessionjobs.flink.apache.org | default | basic-session-job-example                        |
    | flinksessionjobs.flink.apache.org | default | basic-session-job-example2                       |
    | pods                              | default | basic-session-deployment-example-b47fc8744-zx6pf |
    | pods                              | default | basic-session-deployment-example-taskmanager-1-1 |
    | pods                              | default | basic-session-deployment-example-taskmanager-1-2 |
    | pods                              | default | basic-session-deployment-example-taskmanager-1-3 |
    | pods                              | default | basic-session-deployment-example-taskmanager-1-4 |
    | pods                              | default | basic-session-deployment-example-taskmanager-1-5 |
    | pods                              | default | basic-session-deployment-example-taskmanager-1-6 |
    | pods.metrics.k8s.io               | default | basic-session-deployment-example-b47fc8744-zx6pf |
    | pods.metrics.k8s.io               | default | basic-session-deployment-example-taskmanager-1-1 |
    | pods.metrics.k8s.io               | default | basic-session-deployment-example-taskmanager-1-2 |
    | pods.metrics.k8s.io               | default | basic-session-deployment-example-taskmanager-1-3 |
    | pods.metrics.k8s.io               | default | basic-session-deployment-example-taskmanager-1-4 |
    | pods.metrics.k8s.io               | default | basic-session-deployment-example-taskmanager-1-5 |
    | pods.metrics.k8s.io               | default | basic-session-deployment-example-taskmanager-1-6 |
    | replicasets.apps                  | default | basic-session-deployment-example-b47fc8744       |
    | services                          | default | basic-session-deployment-example                 |
    | services                          | default | basic-session-deployment-example-rest            |
    +-----------------------------------+---------+--------------------------------------------------+

