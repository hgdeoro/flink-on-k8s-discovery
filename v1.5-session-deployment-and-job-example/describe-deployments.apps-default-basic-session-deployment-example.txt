Name:                   basic-session-deployment-example
Namespace:              default
CreationTimestamp:      Fri, 28 Jul 2023 11:11:09 +0300
Labels:                 app=basic-session-deployment-example
                        component=jobmanager
                        type=flink-native-kubernetes
Annotations:            deployment.kubernetes.io/revision: 1
                        flinkdeployment.flink.apache.org/generation: 2
Selector:               app=basic-session-deployment-example,component=jobmanager,type=flink-native-kubernetes
Replicas:               1 desired | 1 updated | 1 total | 1 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:           app=basic-session-deployment-example
                    component=jobmanager
                    type=flink-native-kubernetes
  Annotations:      flinkdeployment.flink.apache.org/generation: 2
  Service Account:  flink
  Containers:
   flink-main-container:
    Image:       flink:1.16
    Ports:       8081/TCP, 6123/TCP, 6124/TCP
    Host Ports:  0/TCP, 0/TCP, 0/TCP
    Command:
      /docker-entrypoint.sh
    Args:
      bash
      -c
      kubernetes-jobmanager.sh kubernetes-session 
    Limits:
      cpu:     1
      memory:  2Gi
    Requests:
      cpu:     1
      memory:  2Gi
    Environment:
      _POD_IP_ADDRESS:   (v1:status.podIP)
    Mounts:
      /opt/flink/conf from flink-config-volume (rw)
  Volumes:
   flink-config-volume:
    Type:      ConfigMap (a volume populated by a ConfigMap)
    Name:      flink-config-basic-session-deployment-example
    Optional:  false
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Available      True    MinimumReplicasAvailable
  Progressing    True    NewReplicaSetAvailable
OldReplicaSets:  <none>
NewReplicaSet:   basic-session-deployment-example-b47fc8744 (1/1 replicas created)
Events:
  Type    Reason             Age    From                   Message
  ----    ------             ----   ----                   -------
  Normal  ScalingReplicaSet  6m55s  deployment-controller  Scaled up replica set basic-session-deployment-example-b47fc8744 to 1
