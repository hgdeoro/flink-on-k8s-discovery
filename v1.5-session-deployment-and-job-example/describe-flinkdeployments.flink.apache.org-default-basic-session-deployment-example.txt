Name:         basic-session-deployment-example
Namespace:    default
Labels:       <none>
Annotations:  <none>
API Version:  flink.apache.org/v1beta1
Kind:         FlinkDeployment
Metadata:
  Creation Timestamp:  2023-07-28T08:11:08Z
  Finalizers:
    flinkdeployments.flink.apache.org/finalizer
  Generation:        2
  Resource Version:  1238
  UID:               ed885e69-56e9-44ab-b3e5-cae0d830afeb
Spec:
  Flink Version:  v1_16
  Image:          flink:1.16
  Job Manager:
    Replicas:  1
    Resource:
      Cpu:          1
      Memory:       2048m
  Service Account:  flink
  Task Manager:
    Resource:
      Cpu:     1
      Memory:  2048m
Status:
  Cluster Info:
    Flink - Revision:             fb4d4ef @ 2023-05-17T12:01:35+02:00
    Flink - Version:              1.16.2
    Total - Cpu:                  7.0
    Total - Memory:               15032385536
  Job Manager Deployment Status:  READY
  Job Status:
    Savepoint Info:
      Last Periodic Savepoint Timestamp:  0
      Savepoint History:
  Lifecycle State:  STABLE
  Reconciliation Status:
    Last Reconciled Spec:      {"spec":{"job":null,"restartNonce":null,"flinkConfiguration":null,"image":"flink:1.16","imagePullPolicy":null,"serviceAccount":"flink","flinkVersion":"v1_16","ingress":null,"podTemplate":null,"jobManager":{"resource":{"cpu":1.0,"memory":"2048m","ephemeralStorage":null},"replicas":1,"podTemplate":null},"taskManager":{"resource":{"cpu":1.0,"memory":"2048m","ephemeralStorage":null},"replicas":null,"podTemplate":null},"logConfiguration":null,"mode":null},"resource_metadata":{"apiVersion":"flink.apache.org/v1beta1","metadata":{"generation":2},"firstDeployment":true}}
    Last Stable Spec:          {"spec":{"job":null,"restartNonce":null,"flinkConfiguration":null,"image":"flink:1.16","imagePullPolicy":null,"serviceAccount":"flink","flinkVersion":"v1_16","ingress":null,"podTemplate":null,"jobManager":{"resource":{"cpu":1.0,"memory":"2048m","ephemeralStorage":null},"replicas":1,"podTemplate":null},"taskManager":{"resource":{"cpu":1.0,"memory":"2048m","ephemeralStorage":null},"replicas":null,"podTemplate":null},"logConfiguration":null,"mode":null},"resource_metadata":{"apiVersion":"flink.apache.org/v1beta1","metadata":{"generation":2},"firstDeployment":true}}
    Reconciliation Timestamp:  1690531869870
    State:                     DEPLOYED
Events:                        <none>
