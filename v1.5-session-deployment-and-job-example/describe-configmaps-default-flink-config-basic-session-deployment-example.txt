Name:         flink-config-basic-session-deployment-example
Namespace:    default
Labels:       app=basic-session-deployment-example
              type=flink-native-kubernetes
Annotations:  <none>

Data
====
flink-conf.yaml:
----
blob.server.port: 6124
taskmanager.memory.process.size: 2048m
kubernetes.jobmanager.annotations: flinkdeployment.flink.apache.org/generation:2
kubernetes.internal.jobmanager.entrypoint.class: org.apache.flink.kubernetes.entrypoint.KubernetesSessionClusterEntrypoint
kubernetes.jobmanager.replicas: 1
jobmanager.rpc.address: basic-session-deployment-example.default
web.cancel.enable: false
execution.target: kubernetes-session
jobmanager.memory.process.size: 2048m
kubernetes.taskmanager.cpu: 1.0
kubernetes.service-account: flink
kubernetes.cluster-id: basic-session-deployment-example
taskmanager.rpc.port: 6122
internal.cluster.execution-mode: NORMAL
kubernetes.container.image: flink:1.16
parallelism.default: 1
kubernetes.jobmanager.cpu: 1.0
kubernetes.namespace: default
taskmanager.numberOfTaskSlots: 1
kubernetes.rest-service.exposed.type: ClusterIP
kubernetes.jobmanager.owner.reference: apiVersion:flink.apache.org/v1beta1,kind:FlinkDeployment,uid:ed885e69-56e9-44ab-b3e5-cae0d830afeb,name:basic-session-deployment-example,controller:false,blockOwnerDeletion:true
$internal.flink.version: v1_16

log4j-console.properties:
----
################################################################################
#  Licensed to the Apache Software Foundation (ASF) under one
#  or more contributor license agreements.  See the NOTICE file
#  distributed with this work for additional information
#  regarding copyright ownership.  The ASF licenses this file
#  to you under the Apache License, Version 2.0 (the
#  "License"); you may not use this file except in compliance
#  with the License.  You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
#  Unless required by applicable law or agreed to in writing, software
#  distributed under the License is distributed on an "AS IS" BASIS,
#  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#  See the License for the specific language governing permissions and
# limitations under the License.
################################################################################

# This affects logging for both user code and Flink
rootLogger.level = INFO
rootLogger.appenderRef.console.ref = ConsoleAppender
rootLogger.appenderRef.rolling.ref = RollingFileAppender

# Uncomment this if you want to _only_ change Flink's logging
#logger.flink.name = org.apache.flink
#logger.flink.level = INFO

# The following lines keep the log level of common libraries/connectors on
# log level INFO. The root logger does not override this. You have to manually
# change the log levels here.
logger.akka.name = akka
logger.akka.level = INFO
logger.kafka.name= org.apache.kafka
logger.kafka.level = INFO
logger.hadoop.name = org.apache.hadoop
logger.hadoop.level = INFO
logger.zookeeper.name = org.apache.zookeeper
logger.zookeeper.level = INFO

# Log all infos to the console
appender.console.name = ConsoleAppender
appender.console.type = CONSOLE
appender.console.layout.type = PatternLayout
appender.console.layout.pattern = %d{yyyy-MM-dd HH:mm:ss,SSS} %-5p %-60c %x - %m%n

# Log all infos in the given rolling file
appender.rolling.name = RollingFileAppender
appender.rolling.type = RollingFile
appender.rolling.append = false
appender.rolling.fileName = ${sys:log.file}
appender.rolling.filePattern = ${sys:log.file}.%i
appender.rolling.layout.type = PatternLayout
appender.rolling.layout.pattern = %d{yyyy-MM-dd HH:mm:ss,SSS} %-5p %-60c %x - %m%n
appender.rolling.policies.type = Policies
appender.rolling.policies.size.type = SizeBasedTriggeringPolicy
appender.rolling.policies.size.size=100MB
appender.rolling.strategy.type = DefaultRolloverStrategy
appender.rolling.strategy.max = 10

# Suppress the irrelevant (wrong) warnings from the Netty channel handler
logger.netty.name = org.apache.flink.shaded.akka.org.jboss.netty.channel.DefaultChannelPipeline
logger.netty.level = OFF

# Flink Deployment Logging Overrides
# rootLogger.level = DEBUG


BinaryData
====

Events:  <none>
