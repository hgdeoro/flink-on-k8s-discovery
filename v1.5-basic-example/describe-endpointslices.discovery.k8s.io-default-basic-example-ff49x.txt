Name:         basic-example-ff49x
Namespace:    default
Labels:       app=basic-example
              endpointslice.kubernetes.io/managed-by=endpointslice-controller.k8s.io
              kubernetes.io/service-name=basic-example
              service.kubernetes.io/headless=
              type=flink-native-kubernetes
Annotations:  endpoints.kubernetes.io/last-change-trigger-time: 2023-07-25T09:38:35Z
AddressType:  IPv4
Ports:
  Name            Port  Protocol
  ----            ----  --------
  jobmanager-rpc  6123  TCP
  blobserver      6124  TCP
Endpoints:
  - Addresses:  10.42.0.13
    Conditions:
      Ready:    true
    Hostname:   <unset>
    TargetRef:  Pod/basic-example-5bbd68657d-7ssn7
    NodeName:   k3s-server
    Zone:       <unset>
Events:         <none>
