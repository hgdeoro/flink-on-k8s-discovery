# Submit 'basic' Flink job `flink-kubernetes-operator/release-1.5/examples/basic.yaml`

Logs of pods were dumped to `logs-*txt`.

To deploy locally:

    $ curl https://raw.githubusercontent.com/apache/flink-kubernetes-operator/release-1.5/examples/basic.yaml
    apiVersion: flink.apache.org/v1beta1
    kind: FlinkDeployment
    metadata:
      name: basic-example
    spec:
      image: flink:1.16
      flinkVersion: v1_16
      flinkConfiguration:
        taskmanager.numberOfTaskSlots: "2"
      serviceAccount: flink
      jobManager:
        resource:
          memory: "2048m"
          cpu: 1
      taskManager:
        resource:
          memory: "2048m"
          cpu: 1
      job:
        jarURI: local:///opt/flink/examples/streaming/StateMachineExample.jar
        parallelism: 2
        upgradeMode: stateless

    $ kubectl create -f https://raw.githubusercontent.com/apache/flink-kubernetes-operator/release-1.5/examples/basic.yaml
    flinkdeployment.flink.apache.org/basic-example created

    $ kubectl get pods -w
    NAME                                        READY   STATUS    RESTARTS   AGE
    flink-kubernetes-operator-6b659d866-wdg9q   2/2     Running   0          14m
    basic-example-5bbd68657d-7ssn7              1/1     Running   0          2m56s
    basic-example-taskmanager-1-1               1/1     Running   0          48s

    $ kubectl get events -A -w
    default        0s          Normal    Submit                           flinkdeployment/basic-example                          Starting deployment
    default        0s          Normal    ScalingReplicaSet                deployment/basic-example                               Scaled up replica set basic-example-5bbd68657d to 1
    default        0s          Normal    SuccessfulCreate                 replicaset/basic-example-5bbd68657d                    Created pod: basic-example-5bbd68657d-7ssn7
    default        0s          Normal    Scheduled                        pod/basic-example-5bbd68657d-7ssn7                     Successfully assigned default/basic-example-5bbd68657d-7ssn7 to k3s-server
    default        0s          Normal    Pulling                          pod/basic-example-5bbd68657d-7ssn7                     Pulling image "flink:1.16"
    default        0s          Normal    Pulled                           pod/basic-example-5bbd68657d-7ssn7                     Successfully pulled image "flink:1.16" in 1m59.849697649s (1m59.849713014s including waiting)
    default        0s          Normal    Created                          pod/basic-example-5bbd68657d-7ssn7                     Created container flink-main-container
    default        0s          Normal    Started                          pod/basic-example-5bbd68657d-7ssn7                     Started container flink-main-container
    default        0s          Normal    Scheduled                        pod/basic-example-taskmanager-1-1                      Successfully assigned default/basic-example-taskmanager-1-1 to k3s-server
    default        0s          Normal    Pulled                           pod/basic-example-taskmanager-1-1                      Container image "flink:1.16" already present on machine
    default        0s          Normal    Created                          pod/basic-example-taskmanager-1-1                      Created container flink-main-container
    default        0s          Normal    Started                          pod/basic-example-taskmanager-1-1                      Started container flink-main-container
    default        0s          Normal    JobStatusChanged                 flinkdeployment/basic-example                          Job status changed from RECONCILING to RUNNING
    
The new resources created (resource, namespace, name):

    configmaps                        default flink-config-basic-example     
    deployments.apps                  default basic-example                  
    endpoints                         default basic-example                  
    endpoints                         default basic-example-rest             
    endpointslices.discovery.k8s.io   default basic-example-ff49x            
    endpointslices.discovery.k8s.io   default basic-example-rest-jwncw       
    flinkdeployments.flink.apache.org default basic-example                  
    pods                              default basic-example-5bbd68657d-7ssn7 
    pods                              default basic-example-taskmanager-1-1  
    pods.metrics.k8s.io               default basic-example-5bbd68657d-7ssn7 
    pods.metrics.k8s.io               default basic-example-taskmanager-1-1  
    replicasets.apps                  default basic-example-5bbd68657d       
    services                          default basic-example                  
    services                          default basic-example-rest             

```mermaid
sequenceDiagram
    FlinkOperator->>Kubernetes: get_custom_resourcces()
    Note over FlinkOperator,Kubernetes: operator constantly poll for CR
    Kubernetes-->>FlinkOperator: returns: FlinkDeployment
    FlinkOperator->>FlinkDeployment: describe()
    FlinkDeployment-->>FlinkOperator: description
    FlinkOperator->>Kubernetes: create_pod(JobManager)
    Note over FlinkOperator,Kubernetes: creates new POD to run JobManager
    Kubernetes->>JobManager: create()
    JobManager->>Kubernetes: create_pod(TaskManager)
    Note over JobManager,Kubernetes: creates new POD to run TaskManager
    Kubernetes->>TaskManager: create()
```
