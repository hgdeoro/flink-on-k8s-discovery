Name:              basic-example
Namespace:         default
Labels:            app=basic-example
                   type=flink-native-kubernetes
Annotations:       <none>
Selector:          app=basic-example,component=jobmanager,type=flink-native-kubernetes
Type:              ClusterIP
IP Family Policy:  SingleStack
IP Families:       IPv4
IP:                None
IPs:               None
Port:              jobmanager-rpc  6123/TCP
TargetPort:        6123/TCP
Endpoints:         10.42.0.13:6123
Port:              blobserver  6124/TCP
TargetPort:        6124/TCP
Endpoints:         10.42.0.13:6124
Session Affinity:  None
Events:            <none>
