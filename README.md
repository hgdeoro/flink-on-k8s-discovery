# Bring up k3s on Docker

    $ make k3s-run-rm

Get kubectl config:

    $ make k3s-get-kubeconfig
    $ export KUBECONFIG=.kube-config.yaml

To enable completion in bash:

    $ source <(kubectl completion bash)

# Install Flink operator

https://nightlies.apache.org/flink/flink-kubernetes-operator-docs-release-1.5/docs/try-flink-kubernetes-operator/quick-start/

### 1. Install cert-manager

    $ kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.12.0/cert-manager.yaml
    namespace/cert-manager cr[eated
    customresourcedefinition.apiextensions.k8s.io/certificaterequests.cert-manager.io created
    customresourcedefinition.apiextensions.k8s.io/certificates.cert-manager.io created
    customresourcedefinition.apiextensions.k8s.io/challenges.acme.cert-manager.io created
    customresourcedefinition.apiextensions.k8s.io/clusterissuers.cert-manager.io created
    customresourcedefinition.apiextensions.k8s.io/issuers.cert-manager.io created
    customresourcedefinition.apiextensions.k8s.io/orders.acme.cert-manager.io created
    serviceaccount/cert-manager-cainjector created
    serviceaccount/cert-manager created
    serviceaccount/cert-manager-webhook created
    configmap/cert-manager-webhook created
    clusterrole.rbac.authorization.k8s.io/cert-manager-cainjector created
    clusterrole.rbac.authorization.k8s.io/cert-manager-controller-issuers created
    clusterrole.rbac.authorization.k8s.io/cert-manager-controller-clusterissuers created
    clusterrole.rbac.authorization.k8s.io/cert-manager-controller-certificates created
    clusterrole.rbac.authorization.k8s.io/cert-manager-controller-orders created
    clusterrole.rbac.authorization.k8s.io/cert-manager-controller-challenges created
    clusterrole.rbac.authorization.k8s.io/cert-manager-controller-ingress-shim created
    clusterrole.rbac.authorization.k8s.io/cert-manager-view created
    clusterrole.rbac.authorization.k8s.io/cert-manager-edit created
    clusterrole.rbac.authorization.k8s.io/cert-manager-controller-approve:cert-manager-io created
    clusterrole.rbac.authorization.k8s.io/cert-manager-controller-certificatesigningrequests created
    clusterrole.rbac.authorization.k8s.io/cert-manager-webhook:subjectaccessreviews created
    clusterrolebinding.rbac.authorization.k8s.io/cert-manager-cainjector created
    clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-issuers created
    clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-clusterissuers created
    clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-certificates created
    clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-orders created
    clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-challenges created
    clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-ingress-shim created
    clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-approve:cert-manager-io created
    clusterrolebinding.rbac.authorization.k8s.io/cert-manager-controller-certificatesigningrequests created
    clusterrolebinding.rbac.authorization.k8s.io/cert-manager-webhook:subjectaccessreviews created
    role.rbac.authorization.k8s.io/cert-manager-cainjector:leaderelection created
    role.rbac.authorization.k8s.io/cert-manager:leaderelection created
    role.rbac.authorization.k8s.io/cert-manager-webhook:dynamic-serving created
    rolebinding.rbac.authorization.k8s.io/cert-manager-cainjector:leaderelection created
    rolebinding.rbac.authorization.k8s.io/cert-manager:leaderelection created
    rolebinding.rbac.authorization.k8s.io/cert-manager-webhook:dynamic-serving created
    service/cert-manager created
    service/cert-manager-webhook created
    deployment.apps/cert-manager-cainjector created
    deployment.apps/cert-manager created
    deployment.apps/cert-manager-webhook created
    mutatingwebhookconfiguration.admissionregistration.k8s.io/cert-manager-webhook created
    validatingwebhookconfiguration.admissionregistration.k8s.io/cert-manager-webhook created

    $ kubectl get events -A -w
    cert-manager   0s          Normal    ScalingReplicaSet                deployment/cert-manager-cainjector             Scaled up replica set cert-manager-cainjector-57d6fc9f7d to 1
    cert-manager   0s          Normal    SuccessfulCreate                 replicaset/cert-manager-cainjector-57d6fc9f7d   Created pod: cert-manager-cainjector-57d6fc9f7d-lskp2
    cert-manager   0s          Normal    ScalingReplicaSet                deployment/cert-manager                         Scaled up replica set cert-manager-5f68c9c6dd to 1
    cert-manager   0s          Normal    Scheduled                        pod/cert-manager-cainjector-57d6fc9f7d-lskp2    Successfully assigned cert-manager/cert-manager-cainjector-57d6fc9f7d-lskp2 to k3s-server
    cert-manager   0s          Normal    SuccessfulCreate                 replicaset/cert-manager-5f68c9c6dd              Created pod: cert-manager-5f68c9c6dd-rv8lv
    cert-manager   0s          Normal    Scheduled                        pod/cert-manager-5f68c9c6dd-rv8lv               Successfully assigned cert-manager/cert-manager-5f68c9c6dd-rv8lv to k3s-server
    cert-manager   0s          Normal    ScalingReplicaSet                deployment/cert-manager-webhook                 Scaled up replica set cert-manager-webhook-5b7ffbdc98 to 1
    cert-manager   0s          Normal    SuccessfulCreate                 replicaset/cert-manager-webhook-5b7ffbdc98      Created pod: cert-manager-webhook-5b7ffbdc98-7c5jv
    cert-manager   0s          Normal    Scheduled                        pod/cert-manager-webhook-5b7ffbdc98-7c5jv       Successfully assigned cert-manager/cert-manager-webhook-5b7ffbdc98-7c5jv to k3s-server
    cert-manager   0s          Normal    Pulling                          pod/cert-manager-5f68c9c6dd-rv8lv               Pulling image "quay.io/jetstack/cert-manager-controller:v1.12.0"
    cert-manager   0s          Normal    Pulling                          pod/cert-manager-webhook-5b7ffbdc98-7c5jv       Pulling image "quay.io/jetstack/cert-manager-webhook:v1.12.0"
    cert-manager   0s          Normal    Pulling                          pod/cert-manager-cainjector-57d6fc9f7d-lskp2    Pulling image "quay.io/jetstack/cert-manager-cainjector:v1.12.0"
    cert-manager   0s          Normal    Pulled                           pod/cert-manager-webhook-5b7ffbdc98-7c5jv       Successfully pulled image "quay.io/jetstack/cert-manager-webhook:v1.12.0" in 7.919576348s (7.919586754s including waiting)
    cert-manager   0s          Normal    Created                          pod/cert-manager-webhook-5b7ffbdc98-7c5jv       Created container cert-manager-webhook
    cert-manager   0s          Normal    Started                          pod/cert-manager-webhook-5b7ffbdc98-7c5jv       Started container cert-manager-webhook
    cert-manager   0s          Normal    Pulled                           pod/cert-manager-cainjector-57d6fc9f7d-lskp2    Successfully pulled image "quay.io/jetstack/cert-manager-cainjector:v1.12.0" in 10.91769293s (10.917705292s including waiting)
    cert-manager   0s          Normal    Created                          pod/cert-manager-cainjector-57d6fc9f7d-lskp2    Created container cert-manager-cainjector
    cert-manager   0s          Normal    Started                          pod/cert-manager-cainjector-57d6fc9f7d-lskp2    Started container cert-manager-cainjector
    kube-system    0s          Normal    LeaderElection                   lease/cert-manager-cainjector-leader-election   cert-manager-cainjector-57d6fc9f7d-lskp2_b0b93592-3d71-4b3c-ac11-2d4d3619031f became leader
    cert-manager   0s          Normal    Pulled                           pod/cert-manager-5f68c9c6dd-rv8lv               Successfully pulled image "quay.io/jetstack/cert-manager-controller:v1.12.0" in 12.611380083s (12.611398941s including waiting)
    cert-manager   0s          Normal    Created                          pod/cert-manager-5f68c9c6dd-rv8lv               Created container cert-manager-controller
    cert-manager   0s          Normal    Started                          pod/cert-manager-5f68c9c6dd-rv8lv               Started container cert-manager-controller
    kube-system    0s          Normal    LeaderElection                   lease/cert-manager-controller                   cert-manager-5f68c9c6dd-rv8lv-external-cert-manager-controller became leader

The new resources created (resource, namespace, name):

    configmaps                             cert-manager cert-manager-webhook                     
    configmaps                             cert-manager kube-root-ca.crt                         
    deployments.apps                       cert-manager cert-manager                             
    deployments.apps                       cert-manager cert-manager-cainjector                  
    deployments.apps                       cert-manager cert-manager-webhook                     
    endpoints                              cert-manager cert-manager                             
    endpoints                              cert-manager cert-manager-webhook                     
    endpointslices.discovery.k8s.io        cert-manager cert-manager-jc8sf                       
    endpointslices.discovery.k8s.io        cert-manager cert-manager-webhook-nh92d               
    leases.coordination.k8s.io             kube-system  cert-manager-cainjector-leader-election  
    leases.coordination.k8s.io             kube-system  cert-manager-controller                  
    pods                                   cert-manager cert-manager-5f68c9c6dd-rv8lv            
    pods                                   cert-manager cert-manager-cainjector-57d6fc9f7d-lskp2 
    pods                                   cert-manager cert-manager-webhook-5b7ffbdc98-7c5jv    
    pods.metrics.k8s.io                    cert-manager cert-manager-5f68c9c6dd-rv8lv            
    pods.metrics.k8s.io                    cert-manager cert-manager-cainjector-57d6fc9f7d-lskp2 
    pods.metrics.k8s.io                    cert-manager cert-manager-webhook-5b7ffbdc98-7c5jv    
    replicasets.apps                       cert-manager cert-manager-5f68c9c6dd                  
    replicasets.apps                       cert-manager cert-manager-cainjector-57d6fc9f7d       
    replicasets.apps                       cert-manager cert-manager-webhook-5b7ffbdc98          
    rolebindings.rbac.authorization.k8s.io cert-manager cert-manager-webhook:dynamic-serving     
    rolebindings.rbac.authorization.k8s.io kube-system  cert-manager-cainjector:leaderelection   
    rolebindings.rbac.authorization.k8s.io kube-system  cert-manager:leaderelection              
    roles.rbac.authorization.k8s.io        cert-manager cert-manager-webhook:dynamic-serving     
    roles.rbac.authorization.k8s.io        kube-system  cert-manager-cainjector:leaderelection   
    roles.rbac.authorization.k8s.io        kube-system  cert-manager:leaderelection              
    secrets                                cert-manager cert-manager-webhook-ca                  
    serviceaccounts                        cert-manager cert-manager                             
    serviceaccounts                        cert-manager cert-manager-cainjector                  
    serviceaccounts                        cert-manager cert-manager-webhook                     
    serviceaccounts                        cert-manager default                                  
    services                               cert-manager cert-manager                             
    services                               cert-manager cert-manager-webhook                     

### 2. Install Flink operator

    $ helm repo add flink-operator-repo https://downloads.apache.org/flink/flink-kubernetes-operator-1.5.0/
    "flink-operator-repo" has been added to your repositories

    $ helm install flink-kubernetes-operator flink-operator-repo/flink-kubernetes-operator
    NAME: flink-kubernetes-operator
    LAST DEPLOYED: Tue Jul 25 12:25:18 2023
    NAMESPACE: default
    STATUS: deployed
    REVISION: 1
    TEST SUITE: None

Let's check:

    $ helm list
    NAME                     	NAMESPACE	REVISION	UPDATED                                	STATUS  	CHART                          	APP VERSION
    flink-kubernetes-operator	default  	1       	2023-07-25 12:25:18.493159706 +0300 MSK	deployed	flink-kubernetes-operator-1.5.0	1.5.0      

    $ kubectl get pods 
    NAME                                        READY   STATUS    RESTARTS   AGE
    flink-kubernetes-operator-6b659d866-wdg9q   2/2     Running   0          6m17s

    $ kubectl get events -A -w
    default        0s          Normal    ScalingReplicaSet                deployment/flink-kubernetes-operator            Scaled up replica set flink-kubernetes-operator-6b659d866 to 1
    default        0s          Normal    SuccessfulCreate                 replicaset/flink-kubernetes-operator-6b659d866   Created pod: flink-kubernetes-operator-6b659d866-wdg9q
    default        0s          Normal    Scheduled                        pod/flink-kubernetes-operator-6b659d866-wdg9q    Successfully assigned default/flink-kubernetes-operator-6b659d866-wdg9q to k3s-server
    default        0s          Normal    Issuing                          certificate/flink-operator-serving-cert          Issuing certificate as Secret does not exist
    default        0s          Normal    Generated                        certificate/flink-operator-serving-cert          Stored new private key in temporary Secret resource "flink-operator-serving-cert-7dc6s"
    default        0s          Normal    WaitingForApproval               certificaterequest/flink-operator-serving-cert-cp4td   Not signing CertificateRequest until it is Approved
    default        0s          Normal    Requested                        certificate/flink-operator-serving-cert                Created new CertificateRequest resource "flink-operator-serving-cert-cp4td"
    default        0s          Normal    WaitingForApproval               certificaterequest/flink-operator-serving-cert-cp4td   Not signing CertificateRequest until it is Approved
    default        0s          Normal    WaitingForApproval               certificaterequest/flink-operator-serving-cert-cp4td   Not signing CertificateRequest until it is Approved
    default        0s          Normal    WaitingForApproval               certificaterequest/flink-operator-serving-cert-cp4td   Not signing CertificateRequest until it is Approved
    default        0s          Normal    cert-manager.io                  certificaterequest/flink-operator-serving-cert-cp4td   Certificate request has been approved by cert-manager.io
    default        0s          Normal    CertificateIssued                certificaterequest/flink-operator-serving-cert-cp4td   Certificate fetched from issuer successfully
    default        0s          Normal    Issuing                          certificate/flink-operator-serving-cert                The certificate has been successfully issued
    default        0s          Normal    Pulling                          pod/flink-kubernetes-operator-6b659d866-wdg9q          Pulling image "ghcr.io/apache/flink-kubernetes-operator:be07be7"
    default        0s          Normal    Pulled                           pod/flink-kubernetes-operator-6b659d866-wdg9q          Successfully pulled image "ghcr.io/apache/flink-kubernetes-operator:be07be7" in 1m9.783278638s (1m9.783297146s including waiting)
    default        0s          Normal    Created                          pod/flink-kubernetes-operator-6b659d866-wdg9q          Created container flink-kubernetes-operator
    default        0s          Normal    Started                          pod/flink-kubernetes-operator-6b659d866-wdg9q          Started container flink-kubernetes-operator
    default        0s          Normal    Pulled                           pod/flink-kubernetes-operator-6b659d866-wdg9q          Container image "ghcr.io/apache/flink-kubernetes-operator:be07be7" already present on machine
    default        0s          Normal    Created                          pod/flink-kubernetes-operator-6b659d866-wdg9q          Created container flink-webhook
    default        0s          Normal    Started                          pod/flink-kubernetes-operator-6b659d866-wdg9q          Started container flink-webhook

The new resources created (resource, namespace, name):

    certificaterequests.cert-manager.io    default flink-operator-serving-cert-cp4td               
    certificates.cert-manager.io           default flink-operator-serving-cert                     
    configmaps                             default flink-operator-config                           
    deployments.apps                       default flink-kubernetes-operator                       
    endpoints                              default flink-operator-webhook-service                  
    endpointslices.discovery.k8s.io        default flink-operator-webhook-service-wfdl7            
    issuers.cert-manager.io                default flink-operator-selfsigned-issuer                
    pods                                   default flink-kubernetes-operator-6b659d866-wdg9q       
    pods.metrics.k8s.io                    default flink-kubernetes-operator-6b659d866-wdg9q       
    replicasets.apps                       default flink-kubernetes-operator-6b659d866             
    rolebindings.rbac.authorization.k8s.io default flink-role-binding                              
    roles.rbac.authorization.k8s.io        default flink                                           
    secrets                                default flink-operator-webhook-secret                   
    secrets                                default sh.helm.release.v1.flink-kubernetes-operator.v1 
    secrets                                default webhook-server-cert                             
    serviceaccounts                        default flink                                           
    serviceaccounts                        default flink-operator                                  
    services                               default flink-operator-webhook-service                  

The operator logs contain ANSI escape codes that can be cleand up piping them through `sed -e 's/\x1b\[[0-9;]*m//g'`:

    $ kubectl logs flink-kubernetes-operator | sed -e 's/\x1b\[[0-9;]*m//g' > output.txt
