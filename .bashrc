# This scripts is meant to be sourced from this directory to work properly
if [ ! -e .kube-config.yaml ] ; then
    echo "ERROR: .kube-config.yaml not found in current directory"
else
    source <(kubectl completion bash)
    export KUBECONFIG=$(pwd)/.kube-config.yaml
    export HELM_CACHE_HOME=$(pwd)/.helm/cache
    export HELM_CONFIG_HOME=$(pwd)/.helm/config
    export HELM_DATA_HOME=$(pwd)/.helm/data
fi
