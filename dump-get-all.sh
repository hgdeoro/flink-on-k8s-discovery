#!/bin/bash

set -eu

name="$1"

output="tmp/dump-k8s-$(date +%s)-$name"
for i in $(kubectl api-resources --verbs=list --namespaced -o name | grep -v "events.events.k8s.io" | grep -v "events" | sort | uniq); do
  (
    echo ""
    echo "----------------------------------------------------------------------------------------------------"
    echo "Resource: $i"
    echo "----------------------------------------------------------------------------------------------------"
    echo ""
    kubectl get "${i}" -A -o json | jq -r '.items[].metadata | "\(.namespace),\(.name)"' | sort
    echo ""
  ) | tee -a "${output}.txt"
done

(
  for i in $(kubectl api-resources --verbs=list --namespaced -o name | grep -v "events.events.k8s.io" | grep -v "events" | sort | uniq); do
    (
      kubectl get "${i}" -A -o json | jq -r '.items[].metadata | "\(.namespace)\t\(.name)"' | ts "${i}" | tr " " "\t"
    )
  done
) | sort | tee "${output}.csv"

# kubectl get roles.rbac.authorization.k8s.io -A -o json | jq -r '.items[].metadata | "\(.namespace),\(.name)"' | sort
