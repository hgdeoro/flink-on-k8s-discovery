k3s-run:
	# https://hub.docker.com/r/rancher/k3s/
	docker run \
		--privileged \
		--name k3s-server \
		--hostname k3s-server \
		--network host \
		${DOCKER_K3S_OPTS} \
		rancher/k3s:v1.26.6-k3s1 \
		server

k3s-run-rm:
	docker rm k3s-server || true
	$(MAKE) k3s-run DOCKER_K3S_OPTS="--rm"

k3s-get-kubeconfig:
	@docker cp k3s-server:/etc/rancher/k3s/k3s.yaml ./.kube-config.yaml
	@echo "Remember to set KUBECONFIG."
	@echo "For example:"
	@echo "    export KUBECONFIG=.kube-config.yaml"

k3s-logs:
	docker logs -n 100 -f k3s-server
